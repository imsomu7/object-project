function defaults(testObject, objc) {
    let defaults = {};
    for (let key in testObject) {
        if (objc[key]) {
            defaults[key] = objc[key];
        }
        else {
            defaults[key] = testObject[key];
        }

    }
    return defaults;
}
module.exports = defaults;

