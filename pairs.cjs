function pairs(testobject) {
    const pair = [];
    if (!typeof (testobject) === "object") {
        return [];
    }
    if (!typeof (testobject) === "string") {
        return [];
    }
    const Keys = Object.keys(testobject);
    const Values = Object.values(testobject);
    for (let p = 0; p < Keys.length; p++) {
        pair.push([Keys[p], Values[p]]);
    }
    return pair
}
module.exports = pairs;