function map_object(testobject,obj)
{
    const mapp = {}
    if (!typeof (testobject) === "object")
    {
        return [];
    }
    
    for (let m in testobject)
    {
        mapp[m] = obj(testobject[m]);
    }
    return mapp;
}

module.exports = map_object;